<?php

namespace Notive\EndPointBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SensorController extends Controller
{
    /**
     * @return null
     */
    public function getSensorDataAction()
    {
        return null;
    }

    /**
     * @return null
     */
    public function postSensorDataAction()
    {
        return null;
    }
}
