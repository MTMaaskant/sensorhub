<?php
/**
 * Created by PhpStorm.
 * User: M
 * Date: 29-3-2017
 * Time: 12:27
 */

namespace Notive\EndPointBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SensorController extends Controller
{
    /**
     * @return null
     */
    public function getSensorAction()
    {
        return null;
    }

    /**
     * @return null
     */
    public function postSensorAction()
    {
        return null;
    }
}
