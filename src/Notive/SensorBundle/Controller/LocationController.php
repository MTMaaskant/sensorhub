<?php
/**
 * Created by PhpStorm.
 * User: M
 * Date: 29-3-2017
 * Time: 12:28
 */

namespace Notive\EndPointBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class SensorController extends Controller
{
    /**
     * @return null
     */
    public function getLocationAction()
    {
        return null;
    }

    /**
     * @return null
     */
    public function postLocationAction()
    {
        return null;
    }
}
