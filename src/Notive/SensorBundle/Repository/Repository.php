<?php
/**
 * Created by PhpStorm.
 * User: M
 * Date: 29-3-2017
 * Time: 14:08
 */

namespace Notive\SensorBundle\Repository;

use Doctrine\Common\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\QueryException;
use Symfony\Component\Config\Definition\Exception\Exception;

abstract class Repository extends EntityRepository
{
    /** @var EntityManager $em */
    protected $em;

    /** @var \Doctrine\ORM\Mapping\ClassMetadata $class */
    protected $class;

    /**
     * Repository constructor.
     * @param EntityManager $em
     * @param \Doctrine\ORM\Mapping\ClassMetadata $class
     */
    public function __construct(EntityManager $em, \Doctrine\ORM\Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);

        $this->em = $em;
    }

    /**
     * @param bool $queryBuilder
     * @param null $entity
     * @param null $params
     * @param bool $findOne
     * @return array|bool|mixed|null|object
     */
    protected function get($entity, $params = NULL, $findOne = false, $queryBuilder = false)
    {
        if ($queryBuilder && $params) {
            $qb = $this->em->createQueryBuilder();

            try {
                $qb->select($params['alias']);
                $qb->from('NotiveSensorBundle:' . $entity, $params['alias']);

                $result = $qb;
            } catch(\Exception $e) {
                throw new Exception('Failed to execute query, message: ' . $e->getMessage());
            }
        } else {
            if ($params && $findOne) {
                $result = $this->em->getRepository('NotiveSensorBundle:' . $entity)->findOneBy($params);
            } elseif ($params) {
                $result = $this->em->getRepository('NotiveSensorBundle:' . $entity)->findOneBy($params);
            } else {
                $result = $this->em->getRepository('NotiveSensorBundle:' . $entity)->findAll();
            }
        }

        if ($result) {
            return $result;
        }

        return false;
    }
}