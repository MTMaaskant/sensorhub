<?php

namespace Notive\SensorBundle\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\ClassMetadata;

const ENTITY = 'data';

/**
 * Class DataRepository
 * @package Notive\SensorBundle\Repository
 */
class DataRepository extends Repository
{
    /** @var  EntityManager $em */
    protected $em;

    /** @var  ClassMetadata $class; */
    protected $class;

    /**
     * DataRepository constructor.
     */
    public function __construct($em, $class)
    {
        parent::__construct($em, $class);
    }

    public function getData()
    {
        /** @var \Doctrine\ORM\QueryBuilder $data */
        $data = $this->get(ENTITY, ['alias' => 'd', 'arrayResult' => true], false, true);
        $result = $data->getQuery()->getArrayResult();

        if ($result)
        {
            return $result;
        }


        return false;
    }
}