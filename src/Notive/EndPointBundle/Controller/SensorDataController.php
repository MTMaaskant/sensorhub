<?php

namespace Notive\EndPointBundle\Controller;

use Notive\SensorBundle\Repository\DataRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Encoder\JsonEncode;

class SensorDataController extends Controller
{
    /**
     * @return array|bool|JsonResponse
     */
    public function getDataAction()
    {
        $em = $this->em = $this->getDoctrine()->getManager();

        /** @var DataRepository $dataRepository */
        $dataRepository = $em->getRepository('NotiveSensorBundle:Data');

        $data = $dataRepository->getData();

        if ($data) {
            return new jsonResponse($data);
        }

        return new jsonResponse(json_encode(['msg' => "Something went wrong."]));
    }
}
